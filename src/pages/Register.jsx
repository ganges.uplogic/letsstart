import React, { useState } from "react";
import { FcAddImage } from "react-icons/fc";
import { createUserWithEmailAndPassword, updateProfile } from "firebase/auth";
import { auth, db, storage } from "../firebase";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { collection, doc, setDoc } from "firebase/firestore";
import { Link, useNavigate } from "react-router-dom";

const Register = () => {
  const [Error, setError] = useState(false);
  const nav = useNavigate();
  const handleSubmit = async (e) => {
    e.preventDefault();
    // console.log(e.target[3].files[0]);
    const displayName = e.target[0].value;
    const email = e.target[1].value;
    const password = e.target[2].value;
    const file = e.target[3].files[0];
    console.log(e.target[0].value);

    // try {
      const res = await createUserWithEmailAndPassword(auth, email, password);

      const storageRef = ref(storage, displayName);

      const uploadTask = uploadBytesResumable(storageRef, file);
      uploadTask.on(
        (error) => {
          setError(true);
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
            await updateProfile(res.user, {
              displayName,
              photoURL: downloadURL,
            });
            const newCityRef = doc(collection(db, "users"));
            
            const docData = {
              uid: res.user.uid,
              displayName,
              email,
              photoURL: downloadURL,
            };
            await setDoc(newCityRef, docData);
            const chatsData = {};

            await setDoc(doc(db, "data", res.user.uid), chatsData);
            // await setDoc(doc(db, "userChats", res.user.uid), {});

            // const newCityRef1 = doc(collection(db, "userChats"));
            
            // const data = {};
            // await setDoc(newCityRef1, data);

            nav("/");
          });
        }
      );
      
    // } catch {
    //   setError(true);
    // }
  };
  return (
    <div className="formContainer">
      <div className="formWrapper">
        <span className="logo">Lama</span>
        <span className="title">Register</span>
        <form onSubmit={handleSubmit}>
          <input type={"text"} placeholder="Display Name" />
          <input type={"email"} placeholder="Emal" autoComplete="off" />
          <input type={"password"} placeholder="Password" />
          <input style={{ display: "none" }} type={"file"} id="file" />
          <label htmlFor="file">
            {/* <img src="https://png.pngtree.com/png-vector/20191101/ourmid/pngtree-cartoon-color-simple-male-avatar-png-image_1934459.jpg" alt="Avatar" /> */}
            <FcAddImage size={32} />
            <p>Add an Avatar</p>
          </label>
          <button>Sign Up</button>
          {Error && <span style={{ color: 'red', fontSize: '12px'}}>Something went wrong</span>}
        </form>
        <p>
          You do have an account? <Link to={'/login'}><span className="span">Login</span></Link>
        </p>
      </div>
    </div>
  );
};

export default Register;
