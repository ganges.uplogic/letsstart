import { signInWithEmailAndPassword } from 'firebase/auth';
import React, { useState } from 'react'
import { FcAddImage } from 'react-icons/fc';
import { Link } from 'react-router-dom';
import { auth } from '../firebase';


const Login = () => {
  const [Error, setError] = useState(false)
  const handleSubmit = async (e) => {
    e.preventDefault();
    const email = e.target[0].value;
    const password = e.target[1].value;

    try {
      await signInWithEmailAndPassword(auth, email, password)
    } catch {
      setError(true);
    }
  };
  return (
    <div className='formContainer'>
        <div className='formWrapper'>
            <span className="logo">Lama</span>
            <span className="title">Login</span>
            <form onSubmit={(e) => handleSubmit(e)}>
                <input type={'email'} placeholder='Emal'/>
                <input type={'password'} placeholder='Password'/>
                
                <button>Sign Up</button>
                {Error && <span style={{ color: 'red', fontSize: '12px'}}>Something went wrong</span>}
            </form>
            <p>You don't have an account? <Link to={'/register'}><span className='span'>Register</span></Link></p> 
        </div>
    </div>
  )
}

export default Login