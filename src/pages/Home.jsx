import React, { useContext } from 'react'
import Chat from '../component/Chat'
import Sidebar from '../component/Sidebar'
import { AuthContext } from '../context/AuthContext';

const Home = () => {
  const {currentUser} = useContext(AuthContext);
  console.log("user -->", currentUser);
  return (
    <div className='home'>
       <div className='container'>
        <Sidebar />
        <Chat />
       </div>
    </div>
  )
}

export default Home