import logo from "./logo.svg";
import "./style.scss";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Home from "./pages/Home";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { useContext } from "react";
import { AuthContext, AuthContextProvider } from "./context/AuthContext";

function App() {
  const {currentUser} = useContext(AuthContext);
  console.log("user -->", currentUser);

  // const ProtectedRoute = ({children}) => {
  //   if(!currentUser){
  //     return <Navigate to="/login" />
  //   }
  // }
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/">
          <Route index element={currentUser ? <Home /> : <Login />} />
          <Route path="register" element={<Register />} />
          <Route path="login" element={<Login />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
