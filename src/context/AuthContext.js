import { onAuthStateChanged } from "firebase/auth";
import { createContext, useState, useEffect, Fragment } from "react";
import { auth } from "../firebase";

export const AuthContext = createContext();

export const AuthContextProvider = (props) => {
  const [currentUser, setcurrentUser] = useState({});
  useEffect(() => {
    const user = onAuthStateChanged(auth, (user) => {
      console.log("context update -->", user);
      setcurrentUser(user);
    });

    return () => {
      user();
    };
  }, []);
   console.log(currentUser);
  return (
    <AuthContext.Provider value={{ currentUser }}>
        <Fragment>{props.children}</Fragment>
    </AuthContext.Provider>
  );
};
