import React from 'react'

function Message() {
  return (
    <div className='message owner'>
      <div className='messageInfo'>
        <img className='img' src='https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8cHJvZmlsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=800&q=60' alt='' />
        <span>just now</span>
      </div>
      <div className='messageContent'>
        <p>Hello</p>
        <img className='img' src='https://images.unsplash.com/photo-1497316730643-415fac54a2af?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8cHJvZmlsZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=800&q=60' alt='' />
      </div>
    </div>
  )
}

export default Message