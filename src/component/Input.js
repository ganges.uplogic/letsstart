import React from "react";
import { MdAttachment } from "react-icons/md";
import { MdAddPhotoAlternate } from "react-icons/md";

function Input() {
  return (
    <div className="input">
      <input type={"text"} placeholder={"Type Something... "} />
      <div className="send">
        <MdAttachment className="attach" />
        <input type={"file"} style={{ display: "none" }} id={"file"} />
        <label htmlFor="file">
          <MdAddPhotoAlternate />
        </label>
        <button>Send</button>
      </div>
    </div>
  );
}

export default Input;
