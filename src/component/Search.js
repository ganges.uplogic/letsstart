import { collection, getDocs, query, where } from "firebase/firestore";
import React, { useState } from "react";
import { db } from "../firebase";

const Search = () => {
  const [userName, setuserName] = useState("");
  const [user, setuser] = useState(null);
  const searchFun = async () => {
    console.log(userName);
    console.log("Searching.. ");

    const q = query(collection(db, "users"), where("displayName", "==", userName));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      console.log(doc.data());
      setuser(doc.data())
    });
  }
  const DetailsFun = () => {
    console.log("details")
  }
  return (
    <div className="search">
      <div className="searchForm">
        <input type={"text"} placeholder={'find a user'} onChange={(e) => setuserName(e.target.value)} onKeyDown={(e) => e.code === "Enter" && searchFun()} />
      </div>
      {user && <div className="userChat" onClick={DetailsFun}>
        <img src={user.photoURL} alt="" />
        <div className="usrChatInfo">
          <span>{user.displayName}</span>
        </div>
      </div>}
    </div>
  );
};

export default Search;
