import React from 'react'
import { BsFillCameraVideoFill } from 'react-icons/bs'
import { IoPersonAddSharp } from 'react-icons/io5';
import { TfiMoreAlt } from 'react-icons/tfi'
import Input from './Input';
import Messages from './Messages';

const Chat = () => {
  return (
    <div className='chat'>
      <div className='chatInfo'>
        <span>Jane</span>
        <div className='chatIcons'>
          <BsFillCameraVideoFill size={14} />
          <IoPersonAddSharp size={14} />
          <TfiMoreAlt size={14} />
        </div>
      </div>
      <Messages />
      <Input />
    </div>
  )
}

export default Chat