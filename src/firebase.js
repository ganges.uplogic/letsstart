// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getStorage } from "firebase/storage";
import { getFirestore } from "firebase/firestore";
const firebaseConfig = {
  apiKey: "AIzaSyBmhdr4teq0ZI6z6YGrCy-qwlOKK4maadU",
  authDomain: "letsstart-e521e.firebaseapp.com",
  projectId: "letsstart-e521e",
  storageBucket: "letsstart-e521e.appspot.com",
  messagingSenderId: "179418818177",
  appId: "1:179418818177:web:eddd798e5c088b56a0cf80"
};
// const firebaseConfig = {
//   apiKey: "AIzaSyDx1vLJk7aTJoBMy-cXwQjEw8_LGqEvf6M",
//   authDomain: "lamachat-7e8ca.firebaseapp.com",
//   projectId: "lamachat-7e8ca",
//   storageBucket: "lamachat-7e8ca.appspot.com",
//   messagingSenderId: "164051124286",
//   appId: "1:164051124286:web:03dcf04f67806c7a38516f"
// };


// Initialize Firebase

export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const storage = getStorage();
export const db = getFirestore();